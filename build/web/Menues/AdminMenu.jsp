<%-- 
    Document   : AdminMenu
    Created on : 05-oct-2018, 23:46:21
    Author     : Andrada Flavio
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        
        <link rel="stylesheet" type="text/css" href="css/estilo.css">
        <link rel="icon" type="image/x-icon" href="img/favicon.ico">
        <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
        <title>Menú de administrador</title>
    </head>
    <body>
        <h2>Bienvenido Admin! ${usuarioElegido}</h2>
        <form method="post" action="MenuAdmin">
            <button type="submit" name="button" value="UsuarioAlta">Crear Usuario</button>
            <button  type="submit" name="button" value="UsuarioLista">Listar Usuario</button>
            <button  type="submit" name="button" value="LogOut">Cerrar Sesion</button>
            <input type="hidden" name="dirIP" value="localhost">
            <input type="hidden" name="nomBD" value="ControlAcceso">
        </form>
        
        
    </body>
</html>
