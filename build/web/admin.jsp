<%-- 
    Document   : admin
    Created on : 24-sep-2018, 22:36:40
    Author     : Andrada Flavio
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="icon" type="image/x-icon" href="img/favicon.ico">
        <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
        <title>Login</title>
    </head>
    <body>
        <h2>Bienvenido Admin! ${usuarioElegido}</h2>
        <form method="post" action="MenuAdmin">
            <button type="submit" name="button" value="UsuarioAlta">Crear Usuario</button>
            <button  type="submit" name="button" value="UsuarioLista">Listar Usuario</button>
            <button  type="submit" name="button" value="LogOut">Cerrar Sesion</button>
            <input type="hidden" name="dirIP" value="localhost">
            <input type="hidden" name="nomBD" value="ControlAcceso">
        </form>
        
        
    </body>
</html>