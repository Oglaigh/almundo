<%-- 
    Document   : inicio
    Created on : 30-sep-2018, 4:36:10
    Author     : Andrada Flavio
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="icon" type="image/x-icon" href="img/favicon.ico">
        <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
        <title>Login</title>
    </head>
    <body>
        <div class="container" style="margin-top:20px;">
            <div class="col-md-4 col-md-offset-4" style="border:1px solid #ccc;padding:20px 15px;margin:0 auto;">
                <h1 class = "centrar">Usuario Administrador </h1>
                <form method="post" action="AltaAdministrador">
                    <p class="centrar">
                        <label for="nombre">Nombre</label>
                        <input type = "text" id="nombre" class="form-control" name = "nombre">
                    </p>
                    <p class="centrar">
                        <label for="nombre">Apellido</label>
                        <input type = "text" id="apellido" class="form-control" name = "apellido">
                    </p>
                    <p class="centrar">
                        <label for="nombre">DNI</label>
                        <input type = "text" id="dni" class="form-control" name = "dni">
                    </p>
                    <p class="centrar">
                        <label for="nombre">Número de Teléfono</label>
                        <input type = "text" id="telefono" class="form-control" name = "telefono">
                    </p>
                    <p class="centrar">
                        <label for="password">Contraseña:</label>
                        <input type = "password" id="password" class="form-control" name = "password">
                    </p>
                    <p class="centrar">
                        <button type="submit" name="entrar" class="btn btn-primary">Iniciar sesión</button>
                    </p>
                        <input type="hidden" name="dirIP" value="localhost">
                        <input type="hidden" name="nomBD" value="almundo">
                </form>
            </div>
        </div>
    </body>
</html>


