/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.FactoriaDePersonaDAO;
import modelo.PersonaDAO;
import modelo.UsuarioDAO;
/**
 *
 * @author Andrada Flavio
 */
public class Login extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    HttpServletRequest request;
    HttpServletResponse response;
    
    
    
    private class ExceptionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent event) {
            String exception = event.getActionCommand();
            request.setAttribute("mensajeError", exception);
            RequestDispatcher vista = request.getRequestDispatcher("error.jsp");
            try {
                vista.forward(request, response);
            } catch (ServletException ex) {
                Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
        @Override
    public void doPost(HttpServletRequest request,HttpServletResponse response)
            throws IOException, ServletException {
        this.request = request;
        this.response = response;
        String ip = request.getParameter("dirIP");
        String bd = request.getParameter("nomBD");
        String usuario = request.getParameter("usuario");
        String password = request.getParameter("password");
        UsuarioDAO ud = new UsuarioDAO(ip, bd);
        ud.addExceptionListener(new ExceptionListener());
        boolean isEmpty = ud.consultarTabla();
        ud.consultarUsuario(usuario,password);
        
        if(isEmpty){
            request.setAttribute("usuarios", ud.getResultado());
            request.setAttribute("usuarioElegido", usuario);
        
            RequestDispatcher vista = request.getRequestDispatcher("inicio.jsp");
            vista.forward(request, response);
       }
       else{
           PersonaDAO personaDAO = FactoriaDePersonaDAO.getInstance().crearPersonaDAO(ud.getResultado().getPerfil());
           personaDAO.crearPersona(usuario,password);
           personaDAO.mostrarMenu(request,response);
       }
       
        
        
    }
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
