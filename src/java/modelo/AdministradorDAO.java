/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Andrada Flavio
 */
public class AdministradorDAO extends PersonaDAO{

    private AdministradorDTO resultado;
    private String jdbcDriver;
    private String dbName;
    private String urlRoot;
    private ActionListener listener;

    
    
    public AdministradorDAO(String url, String dbName) {
        super(url, dbName);
    }
    
   
    public AdministradorDAO(){}
    
    @Override
    public void crearPersona(String nombre, String password) {
        AdministradorDTO administradores = new AdministradorDTO();
        try{
            Connection con = DriverManager.getConnection(getUrlRoot() + getDbName(), "", "");
            Statement stmt = con.createStatement();
            stmt.execute(
                    "SELECT id_persona,persona_nombre,persona_apellido,persona_dni,persona_telefono "
                            + "FROM persona "
                            + "WHERE id_persona = (SELECT rela_persona "
                                                   + "FROM perfil "
                                                   + "WHERE perfil_usuario ='" + nombre +"' "
                                                   + "AND perfil_apellido = '" + password + "';");
            ResultSet rs = stmt.getResultSet();
            while (rs.next()){
                AdministradorDTO admin = new AdministradorDTO();
                admin.setId(Integer.parseInt(rs.getString(1)));
                admin.setNombre(rs.getString(2));
                admin.setApellido(rs.getString(3));
                admin.setDni(rs.getString(4));
                admin.setTelefono(rs.getString(5));
                resultado = admin;
            }
            con.close();
        }catch(Error e){
            System.err.println(e.getMessage());
        } catch (SQLException ex) {
            Logger.getLogger(AdministradorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mostrarMenu(HttpServletRequest request, HttpServletResponse response) {
        RequestDispatcher vista = request.getRequestDispatcher("admin.jsp");
        try {
            vista.forward(request, response);
        } catch (ServletException ex) {
            Logger.getLogger(AdministradorDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(AdministradorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

  

    
    
}
