/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Andrada Flavio
 */
public class PerfilDAO extends DAOFactory{

    private PerfilDTO resultado;
    public PerfilDAO(String url, String dbName) {
        super(url, dbName);
    }
    
    public void consultarPerfil(int id) {
        try {
            Connection con = DriverManager.getConnection(super.getUrlRoot() + super.getDbName(), "", "");
            Statement stmt = con.createStatement();
            stmt.execute(
                    "SELECT id_perfil,perfil_descr "
                            + "FROM Perfil "
                            + "WHERE id_perfil=" + id + ";");
            ResultSet rs = stmt.getResultSet();
            while (rs.next()){
                PerfilDTO perfil = new PerfilDTO();
                perfil.setId(Integer.parseInt(rs.getString(1)));
                perfil.setDescr(rs.getString(2));
                resultado = perfil;
            }
            con.close();
        } catch (SQLException e) {
            reportException(e.getMessage());
        }

    }
}
