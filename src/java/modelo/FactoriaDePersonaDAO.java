/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Andrada Flavio
 */
public class FactoriaDePersonaDAO {
    private static FactoriaDePersonaDAO f = null;
        
    public static FactoriaDePersonaDAO getInstance() {
        if (f == null) {
            f = new FactoriaDePersonaDAO();
        }
        return f;
    }
    
    public PersonaDAO crearPersonaDAO(String perfil) {
        try {
            return (PersonaDAO) Class.forName(f.getClass().getPackage().getName() + "." + perfil+"DAO").newInstance();
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            System.err.println(ex);
        }
        return null;
    }
    
}
