/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.Collection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Andrada Flavio
 */
public abstract class PersonaDAO extends DAOFactory{

    public PersonaDAO(String url, String dbName) {
        super(url, dbName);
    }
    
    public PersonaDAO(){}
    
    
    public abstract void crearPersona(String usuario,String password);
    //To change body of generated methods, choose Tools | Templates.

    public abstract void mostrarMenu(HttpServletRequest request, HttpServletResponse response);
    //To change body of generated methods, choose Tools | Templates.
    }
