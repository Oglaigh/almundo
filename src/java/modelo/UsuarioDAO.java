/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Andrada Flavio
 */
public class UsuarioDAO extends DAOFactory{
    
    private UsuarioDTO resultado;
    boolean empty = true;
    public UsuarioDAO(String url, String dbName) {
        super(url, dbName);
    }
    
    public boolean consultarTabla() {
        try {
            Connection con = DriverManager.getConnection(super.getUrlRoot() + super.getDbName(), "", "");
            Statement stmt = con.createStatement();
            stmt.execute(
                    "SELECT id_usuario,usuario_nombre, usuario_password, rela_persona,rela_perfil "
                            + "FROM usuario;");
            ResultSet rs = stmt.getResultSet();
            while (rs.next()){
                empty = false;
            }
            con.close();
        } catch (SQLException e) {
            reportException(e.getMessage());
        }
        return empty;
    }
    
     public void consultarUsuario(String u,String p) {
        try {
            Connection con = DriverManager.getConnection(super.getUrlRoot() + super.getDbName(), "", "");
            Statement stmt = con.createStatement();
            stmt.execute(
                    "SELECT id_usuario, usuario_nombre ,usuario_password, rela_persona,perfil_descr "
                            + "FROM usuario "
                            + "INNER JOIN perfil "
                            + "ON usuario.rela_perfil = perfil.id_perfil "
                            + "WHERE usuario.usuario_nombre='" + u +"' AND usuario.usuario_password= '"+p+"';");
            ResultSet rs = stmt.getResultSet();
            while (rs.next()){
                UsuarioDTO usuario = new UsuarioDTO();
                usuario.setId(Integer.parseInt(rs.getString(1)));
                usuario.setUser(rs.getString(2));
                usuario.setPass(rs.getString(3));
                usuario.setRela_Persona(Integer.parseInt(rs.getString(4)));
                usuario.setPerfil(rs.getString(5));
                resultado = usuario;
            }
            con.close();
        } catch (SQLException e) {
            reportException(e.getMessage());
        }

    }
     
     public void CrearUsuario(String nombre,String apellido,int dni,String password,String telefono){
         
         try {
            int rela_persona;
            Connection con = DriverManager.getConnection(super.getUrlRoot() + super.getDbName(), "", "");
            Statement stmt = con.createStatement();
            stmt.execute(
                    "INSERT INTO persona (persona_nombre, persona_apellido, persona_dni,persona_telefono) VALUES ('"+nombre.toUpperCase()+"', '" + apellido.toUpperCase()+ "', "+ dni +","+telefono+ ");");
            stmt.close();
            Statement stmt2 = con.createStatement();
            stmt2.execute("SELECT id_persona FROM persona WHERE persona_nombre = '"+nombre+"' AND persona_apellido = '"+apellido+"';");
            ResultSet rs = stmt2.getResultSet();
            rs.next();
            rela_persona = rs.getInt("id_persona");
            stmt2.execute("INSERT INTO usuario (usuario_nombre, usuario_password, rela_persona,rela_perfil) VALUES ('"+nombre.toLowerCase()+"', '" + password + "', "+ rela_persona + ","+ 1 +");");
            
            con.close();
        } catch (SQLException e) {
            reportException(e.getMessage());
        }
     }
     
     public UsuarioDTO getResultado() {
        return resultado;
    }
}
