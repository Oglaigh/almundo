/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author Andrada Flavio
 */
public class DAOFactory {
    private String jdbcDriver;
    private String dbName;
    String urlRoot;
    private ActionListener listener;

    public DAOFactory(String url, String dbName) {
        jdbcDriver = "com.mysql.jdbc.Driver";
        urlRoot = "jdbc:mysql://" + url + "/";
        this.dbName = dbName;
        listener = null;
        try {
            Class.forName(jdbcDriver);
        } catch (ClassNotFoundException e) {
            reportException(e.getMessage());
        }
    }
    
    public DAOFactory(){}
    
    
    public void reportException(String exception) {
        if (listener != null) {
            ActionEvent evt = new ActionEvent(this, 0, exception);
            listener.actionPerformed(evt);
        }
    }
    
    public void addExceptionListener(ActionListener listener) {
        this.listener = listener;
    }

    public String getJdbcDriver() {
        return jdbcDriver;
    }

    public String getDbName() {
        return dbName;
    }

    public String getUrlRoot() {
        return urlRoot;
    }
    
    
}
