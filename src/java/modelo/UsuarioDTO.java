/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;

/**
 *
 * @author Andrada Flavio
 */
public class UsuarioDTO implements Serializable{
    private int id;
    private String usuario;
    private String pass;
    private int rela_persona;
    private String perfil;

    public String getUser() {
        return usuario;
    }

    public void setUser(String usuario) {
        this.usuario = usuario;
    }

    public String getPass() {
        return pass;
    }
    
    
    public void setPass(String pass) {
        this.pass = pass;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int rol) {
        this.id = id;
    }
    
    public int getRela_persona() {
        return rela_persona;
    }

    public void setRela_Persona(int rela_persona) {
        this.rela_persona = rela_persona;
    }
    
    public String getPerfil() {
        return perfil;
    }

    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }
    
    
}
